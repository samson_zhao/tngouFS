<#include "header.ftl">

<hr>

<div class="am-container">


<div class="am-panel am-panel-secondary">
<div class="am-panel-hd"> <a href="${Domain.base}/show/${gallery.id}"> ${gallery.title} </a>[${gallery.size}张]</div>
<div class="am-panel-bd">
  
<div class="am-fr">
 <#assign img=Domain.imgurl+"/img"+gallery.img>
<#include "../common/share.ftl"></div>
<div class="am-u-sm-12  am-u-md-8 am-u-lg-8 am-u-sm-centered">
  

    <#list list as item>
     <img  class="am-img-responsive am-img-thumbnail" src="${Domain.imgurl}/img${item.src}" alt="${gallery.title}${item_index+1}">    
     </#list>
  
</div>

</div>


<ul class="am-pagination">
  
<#if last??>
              <li class="am-pagination-prev"><a href="${Domain.base}/show/${last.id}">« ${last.title}</a></li>            
</#if>
<#if next??>
<li class="am-pagination-next"><a href="${Domain.base}/show/${next.id}">${next.title} »</a></li>
</#if>		

</ul>


</div>

<footer class="am-comment-footer">
        <div class="am-comment-actions">
        
        <a href="" title="浏览数"><i class="am-icon-eye"></i> ${gallery.count}</a>
        <a href="" title="关注数"><i class="am-icon-star"></i> ${gallery.fcount}</a>
         <a href="#link-to-comment" title="回复数"><i class="am-icon-reply"></i> ${gallery.rcount}</a>
         <time datetime="${gallery.time}" title="${gallery.time}">
    <span class="am-icon-clock-o"></span> ${gallery.time?string("yyyy-MM-dd")}</time>
         </div>
         </footer>

</div>

  





</div>


<#include "footer.ftl">