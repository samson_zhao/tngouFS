<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>登录界面   | 91利人后台登录</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="alternate icon" type="image/png" href="${Domain.base}/common/amazeui/i/favicon.png">
  <link rel="stylesheet" href="${Domain.base}/common/amazeui/css/amazeui.min.css"/>
  <style>
    .header {
      text-align: center;
    }
    .header h1 {
      font-size: 200%;
      color: #333;
      margin-top: 30px;
    }
    .header p {
      font-size: 14px;
    }
  </style>
</head>
<body>
<div class="header">
  <div class="am-g">
   <br>
    <br>
   
   <div class="am-footer-switch">
    <div id="godesktop" data-rel="desktop" class="am-footer-desktop">天狗-TngouFS图片</div>
  </div>

  </div>
  <hr />
</div>
<div class="am-g">
  <div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
   
  <p class="am-text-danger">${msg!}</p>
   

    <form method="post" class="am-form" action="${Domain.base}/login">
      <label for="account">账户:</label>
      <input type="text" name="account"  value="">
      <br>
      <label for="password">密码:</label>
      <input type="password" name="password"  value="">
      <br>
    
      <label for="remember-me">
      账户:tngou  密码：tngoufs
      </label>
     
      <br />
      <div class="am-cf">
        <input type="submit" name="sub" value="登 录" class="am-btn am-btn-primary am-btn-sm am-fl">
        <!--
        <input type="submit" name="" value="忘记密码 ^_^? " class="am-btn am-btn-default am-btn-sm am-fr">
        -->
      </div>
    </form>
    <hr>
    <p>©2015 Tngou.NET </p>
  </div>
</div>
</body>
</html>