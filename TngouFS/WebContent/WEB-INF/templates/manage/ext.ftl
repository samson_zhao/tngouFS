<#include "header.ftl">

  <!-- content start -->
  <div class="admin-content">

    <div class="am-cf am-padding">

  <ol class="am-breadcrumb">
  <li><a href="${Domain.manage}">首页</a></li>

  <li class="am-active">添加</li>
</ol>
   </div>

  <hr>
  
<div class="am-g">

    

 
        <form  id="form_get" class="am-form am-form-horizontal" action="${Domain.base}/ext/jsonimg">
          <div class="am-form-group">
            <label  class="am-u-sm-3 am-form-label">URL</label>
            <div class="am-u-sm-9">
              <input name="url" id="url" placeholder="完整的URL地址" type="text">
              <small>完整的URL地址，以http://开通</small>
            </div>
          </div>

          <div class="am-form-group">
            <label  class="am-u-sm-3 am-form-label">前缀</label>
            <div class="am-u-sm-9">
              <input name="prefix" id="prefix" placeholder="" type="text">
           
            </div>
          </div>

          <div class="am-form-group">
            <label  class="am-u-sm-3 am-form-label">后缀</label>
            <div class="am-u-sm-9">
              <input name="suffix" id="suffix" placeholder="png/jpg" type="text">
            </div>
          </div>
          <div class="am-form-group">
            <label  class="am-u-sm-3 am-form-label">过滤后缀</label>
            <div class="am-u-sm-9">
              <input name="filtersuffix" id="filtersuffix" placeholder="" type="text">
            </div>
          </div>
         
          <div class="am-form-group">
            <div class="am-u-sm-9 am-u-sm-push-3">
              <button type="submit" class="am-btn am-btn-primary">取得图片</button>
            </div>
          </div>
        </form>

  <script type='text/javascript'>

$(document).ready(function() {

 $('#form_get').ajaxForm({
			dataType: 'json',
	        success: function(json) {
		    
		    
		    if(json.title=="0")
		    {
		    alert("数据爬取错误！");
		    
		    }else
		    {
		    	$("#title").val(json.title);
		    	
		    	$("#form_save").css("display","");
		    	var list=json.list;
		    	for(var i=0;i<list.length;i++){
				
					$("#tbody").prepend("<tr><td><input type='checkbox'  checked='checked' value='"+list[i]+"' name='srcs'></td><td><img src='"+list[i]+"' class='am-thumbnail'></img></td> </tr>");
				
				}
				} 
				
	        }
	    });



 $('#form_save').ajaxForm({
			
	        success: function(id) {
		    if(id!='0')
		    {
		    	location.href="${Domain.manage}/ext";
		    }else
		    {
		    
		    	alert("保存失败！");
		    }
		    
		  }
	    });
});

</script>   
     
     <hr>
        <form class="am-form am-form-horizontal" id="form_save"  style="display:none"  action="${Domain.manage}/ext/save" method="post">
         
         
           <div class="am-form-group">
		      <label >图库分类</label>
		      <select  name="galleryclass">
		      	<#list galleryclasses as item>
		      	<option value="${item.id}">${item.name}</option>
            
            </#list>
		        
		     
		      </select>
		      <span class="am-form-caret"></span>
		    </div>
         
          <div class="am-form-group">
            <label  >标题</label>
            <div >
              <input id="title" name="title" placeholder="图库标题" type="text">
             
            </div>
          </div>


		 <div class="am-form-group">
          
            <div >
            
            <table class="am-table am-table-striped am-table-hover">
            <thead>
              <tr>
                <th ></th>
                <th >图片</th>
              </tr>
             </thead>
          <tbody  id="tbody"></tbody>
           </table>
         </div>
         
           
          </div>
          
          
          
          <div class="am-form-group">
            <div class="am-u-sm-9 am-u-sm-push-3">
              <button type="submit" class="am-btn am-btn-primary"  name="sub" value="提交">保存图片</button>
            </div>
          </div>
        
        </form>

    

  </div>
  <!-- content end -->

<#include "footer.ftl">
