<#include "header.ftl">

  <div class="admin-content">

  
   <div class="am-cf am-padding">

  <ol class="am-breadcrumb">
  <li><a href="${Domain.manage}">首页</a></li>
  <li><a href="${Domain.manage}/classify">图库分类</a></li>
  <li class="am-active">编辑</li>
</ol>
   </div>



   <hr>
   
   <div class="am-g">

    

 
        <form id="form_save " class="am-form am-form-horizontal" action="${Domain.manage}/classify/edit">
         
            <input name="id" value="${galleryclass.id}" type="hidden">
          <div class="am-form-group">
            <label class="am-u-sm-3 am-form-label">名称</label>
            <div class="am-u-sm-9">
              <input name="name"  placeholder="名称" type="text" value="${galleryclass.name}">
            </div>
          </div>

		<div class="am-form-group">
            <label class="am-u-sm-3 am-form-label">排序</label>
            <div class="am-u-sm-9">
              <input name="seq"  placeholder=1,2,3……" type="text" value="${galleryclass.seq}">
            </div>
          </div>
          <div class="am-form-group">
            <label class="am-u-sm-3 am-form-label">标题</label>
            <div class="am-u-sm-9">
              <input name="title"  placeholder="标题" type="text" value="${galleryclass.title}">
           
            </div>
          </div>

          <div class="am-form-group">
            <label class="am-u-sm-3 am-form-label">关键词</label>
            <div class="am-u-sm-9">
              <input name="keywords"  placeholder="关键词" type="text" value="${galleryclass.keywords}">
            </div>
          </div>

 		<div class="am-form-group">
            <label class="am-u-sm-3 am-form-label">描述</label>
            <div class="am-u-sm-9">
             <textarea class="" rows="5" name="description" placeholder="输入分类简介">${galleryclass.description}</textarea>
            </div>
          </div>
         
          <div class="am-form-group">
            <div class="am-u-sm-9 am-u-sm-push-3">
              <button type="submit" class="am-btn am-btn-primary"  name="sub" value="save">修改</button>
            </div>
          </div>
        </form>

  
     
    
    

  </div>
   
   
   
  </div>
<#include "footer.ftl">
