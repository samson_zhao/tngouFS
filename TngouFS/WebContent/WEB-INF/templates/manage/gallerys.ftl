<#include "header.ftl">

  <div class="admin-content">

    <div class="am-cf am-padding">
    <ol class="am-breadcrumb">
  <li><a href="${Domain.manage}">首页</a></li>
  <li><a href="${Domain.manage}/gallery/list/${galleryclass.id}">${galleryclass.name}</a></li>
  <li class="am-active">列表</li>
</ol>
    </div>

<hr>

    <ul class="am-avg-sm-2 am-avg-md-4 am-avg-lg-6 am-margin gallery-list">
    
    <#list page.list as item>
      <li>
        <a href="${Domain.manage}/gallery/show/${item.id}">
  
          <img class="am-img-thumbnail am-img-bdrs" src="${Domain.imgurl}/img${item.img}" alt="">
               <div class="gallery-title">${item.title}</div>
           </a>
          <div class="gallery-desc">${item.size}张  &nbsp;&nbsp;
<a href="${Domain.manage}/gallery/edit?id=${item.id}" title="编辑"><i class="am-icon-pencil"></i></a>
<a href="javascript:deleteGallery('${item.id}');"   title="删除"><i class="am-close  am-close-spin">&times;</i></a></div>
      
      </li>
      </#list>
    </ul>

    <div class="am-margin am-cf">
      <hr>
      
    
    <#include "../common/page.ftl">
    
    </div>

  </div>




<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm-Picture">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><i class="am-icon-question"></i>-天狗提示</div>
    <div class="am-modal-bd">
      你，确定要删除该图片吗？删除不能恢复，请慎重选择！
    </div>
    <div class="am-modal-footer">
      <span class="am-modal-btn" data-am-modal-cancel>取消</span>
      <span class="am-modal-btn" data-am-modal-confirm>确定</span>
    </div>
  </div>
</div>

<script type="text/javascript">

function deleteGallery(vid)
{

 	$('#my-confirm-Picture').modal({

		//确定
	 	onConfirm: function(options) {
           
          	$.get("${Domain.manage}/gallery/delete?id="+vid,
				function(rurl){
						
						location.href=rurl;
					
				});
        },
        //否
        onCancel: function() {
         
        }
	 });
	

}

</script>


<#include "footer.ftl">
