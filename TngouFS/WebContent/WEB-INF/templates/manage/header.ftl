<!doctype html>
<html class="no-js">
<head>
 <title>${title}</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="${keywords}">
	<meta name="description" content="${description}">
	<meta name="author" content="${author}"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${Domain.base}/common/amazeui/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${Domain.base}/common/amazeui/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${Domain.base}/common/amazeui/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${Domain.base}/common/amazeui/css/admin.css">
    
  <script type="text/javascript" src="${Domain.base}/common/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${Domain.base}/common/js/jquery.form.min.js"></script>
  
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，TngouFS 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>TngouFS 文件系统</strong> <small>后台管理模板</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

  <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

    <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list">
      
      <li ><a href="${Domain.base}"><span class="am-icon-home"></span> 前台首页</a></li>
      <li class="am-hide-sm-only"><a href="javascript:;" id="admin-fullscreen"><span class="am-icon-arrows-alt"></span> <span class="admin-fullText">开启全屏</span></a></li>
    </ul>
  </div>
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
    <div class="am-offcanvas-bar admin-offcanvas-bar">
      <ul class="am-list admin-sidebar-list">
        <li><a href="${Domain.manage}"><span class="am-icon-home"></span> 首页</a></li>
        <li class="admin-parent">
          <a class="am-cf" data-am-collapse="{target: '#collapse-nav'}"><span class="am-icon-file"></span> 图库分类 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
          <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav">
          	<#list galleryclasses as item>
            <li><a href="${Domain.manage}/gallery/list/${item.id}"> ${item.name}</a></li>
            </#list>
          </ul>
        </li>
        <li><a href="${Domain.manage}/ext"> <span class="am-icon-bug"></span> 爬取图片</a></li>
         <li><a href="${Domain.manage}/classify"> <span class="am-icon-th-list"></span> 图库分类</a></li>
        <li><a href="${Domain.base}/login/exit"><span class="am-icon-sign-out"></span> 注销</a></li>
      </ul>

      <div class="am-panel am-panel-default admin-sidebar-panel">
        <div class="am-panel-bd">
          <p><span class="am-icon-bookmark"></span> 公告</p>
          <p>打造开源图片服务器。——Tngou</p>
        </div>
      </div>

      
    </div>
  </div>
  <!-- sidebar end -->