package net.tngou.tnfs.action;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;

import net.tngou.tnfs.service.ImgService;
import net.tngou.tnfs.util.HttpConfig;
import net.tngou.tnfs.util.Img4s;
import net.tngou.tnfs.util.ImgTool;
import net.tngou.tnfs.util.ImgUtil;
import net.tngou.tnfs.util.PathFormat;

/**
 * 
* @author tngou@tngou.com
* @date 2014年12月30日 上午10:27:43
*
 */
public class ControllerAction extends BaseAction {

	
	
	@Override
	public void execute() throws ServletException, IOException {
		
		//转换直接调用的是百度的后台接口
		printJson(new ActionEnter( request ).exec()); 
		
		
	}
	
	
	
	public void imgsearch()  {
		
				String q=request.getParameter("q");  //关键词
				String path=request.getParameter("path");
				if(StringUtils.isEmpty(path))path=File.separator+"default"+File.separator;
				String name=request.getParameter("name");  //
				String title=request.getParameter("title");  //
				if(StringUtils.isEmpty(title))title=q;
				String message=request.getParameter("message");  //
				if(StringUtils.isEmpty(message))message=q;
				String tag=request.getParameter("tag");  //
				List<String> urls = new Img4s().getImgUrl(q);
				if(urls==null)
				{
					String json="{\"code\":404}";//错误
					printJson(json);
					return ;
				}
				if(urls.size()==0)
				{
					String json="{\"code\":404}";//错误
					printJson(json);
					return ;
				}
				
					
				
				if(!path.startsWith(File.separator))path=File.separator+path;
				if(!path.endsWith(File.separator))path=path+File.separator;
				HttpConfig httpConfig = HttpConfig.getInstance();
				String savePath =httpConfig.getTnfspath()+File.separator+"img"+path;
				String saveUrl = path;
				String url=null;
				for (String imgurl : urls) {
					url = ImgUtil.DownImg(imgurl, savePath, saveUrl,name);
					if(url!=null)break;
				}
				
//				ImgService imgService = new ImgService();
//				imgService.saveDB(url, q, q, tag);
				String json="{\"code\":404}";//错误
				if(url!=null)
				{
					/**
					 * code 200 返回成功
					 * url ：图片URL简略地址
					 * fullurl：图片URL完整地址
					 * path ：图片服务器地址
					 */
					json="{\"code\":200,\"url\":\""+PathFormat.format(url)+"\",\"fullurl\":\""+httpConfig.getImgurl()+PathFormat.format(url)+"\",\"tnfs\":\""+httpConfig.getTnfsurl()+"\"}";
				}
				
				printJson(json);
	}
	
	
	/**
	 * 图片下载
	 */
	public void imgdownload() {
		
				String imgurl=request.getParameter("imgurl");  //鍥剧墖URL
				String name=request.getParameter("name");  //
				String path=request.getParameter("path");
				if(StringUtils.isEmpty(path))path=File.separator+"default"+File.separator;
				String title=request.getParameter("title");  //
				String message=request.getParameter("message");  //
				String tag=request.getParameter("tag");  //
				if(!path.startsWith(File.separator))path=File.separator+path;
				if(!path.endsWith(File.separator))path=path+File.separator;
				HttpConfig httpConfig = HttpConfig.getInstance();
				String savePath =httpConfig.getTnfspath()+File.separator+"img"+path;
				String saveUrl = path;
				
				
				String url = ImgUtil.DownImg(imgurl, savePath, saveUrl,name);
				
//				ImgService imgService = new ImgService();
//				imgService.saveDB(url, title, message, tag);
				
				String json="{\"code\":404}";//错误
				if(url!=null)
				{
					/**
					 * code 200 返回成功
					 * url ：图片URL简略地址
					 * fullurl：图片URL完整地址
					 * path ：图片服务器地址
					 */
					json="{\"code\":200,\"url\":\""+PathFormat.format(url)+"\",\"fullurl\":\""+httpConfig.getImgurl()+PathFormat.format(url)+"\",\"tnfs\":\""+httpConfig.getTnfsurl()+"\"}";
				}
				
				printJson(json);
	}
	
	
	public void imgcut() {
    	int startX =request.getParameter("startX")==null?0:Integer.parseInt(request.getParameter("startX"));
        int startY = request.getParameter("startY")==null?0:Integer.parseInt(request.getParameter("startY"));
        int endX = request.getParameter("endX")==null?0:Integer.parseInt(request.getParameter("endX"));
        int endY =request.getParameter("endY")==null?0:Integer.parseInt(request.getParameter("endY"));
        int size =request.getParameter("size")==null?200:Integer.parseInt(request.getParameter("size"));
        String contextPath=request.getParameter("path");
        if(StringUtils.isEmpty(contextPath))contextPath=File.separator+"default";
        if(!contextPath.startsWith(File.separator))contextPath=File.separator+contextPath; //File.separator =windows是\，unix是/	
        String name=request.getParameter("name");
        if(StringUtils.isEmpty(name))name=File.separator+new Date().hashCode();
        if(!name.startsWith(File.separator))name=File.separator+name; //File.separator =windows是\，unix是/	
        String src = request.getParameter("src");
        HttpConfig httpConfig = HttpConfig.getInstance();
        src=StringUtils.remove(src, httpConfig.getImgurl());
        
        src=StringUtils.replace(src, "/", File.separator);
        src=File.separator+"img"+src;
        src=httpConfig.getTnfspath()+src;
        
        ImgTool imgTool = new ImgTool();
        BufferedImage bufferedImage = imgTool.image(src);
        if(endX==0)endX=bufferedImage.getWidth();
        if(endY==0)endY=bufferedImage.getHeight();
        bufferedImage=imgTool.crop(bufferedImage, startX, startY, endX, endY);
        
        bufferedImage= imgTool.crop(bufferedImage, size, size,true); //等比压缩
       
        
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyMMdd");
		LocalDate date =LocalDate.now();
        String ymd = date.format(f);
        String pathname=contextPath+File.separator+ymd+name+".png";
        String savename=httpConfig.getTnfspath()+File.separator+"img"+pathname;
		File file = new File(savename);
		file=imgTool.save(bufferedImage, file );
		
		String json="{\"code\":404}";//错误
		if(file.isFile())
		{
			/**
			 * code 200 返回成功
			 * url ：图片URL简略地址
			 * fullurl：图片URL完整地址
			 * path ：图片服务器地址
			 */
			String url=pathname;
			json="{\"code\":200,\"url\":\""+PathFormat.format(url)+"\",\"fullurl\":\""+httpConfig.getImgurl()+PathFormat.format(url)+"\",\"tnfs\":\""+httpConfig.getTnfsurl()+"\"}";
//		    new File(src).delete();
		
		}
		
		printJson(json);
	}
	
	
	
	
	
}
